package mwclient

// site.go includes queries regarding Site info

import (
	"strings"

	"github.com/Masterminds/semver/v3"
	"github.com/pkg/errors"
)

// Version returns the semantic version (https://github.com/Masterminds/semver)
// of the MediaWiki server the Client is tied to.
func (w *Client) Version() (*semver.Version, error) {
	// perform a get query...
	response, err := w.Get(map[string]string{
		"action": "query",
		"meta":   "siteinfo",
		"siprop": "general",
	})
	if err != nil {
		return nil, errors.Wrap(err, "version: could not send GET request")
	}

	rawSemanticVersion, err := response.GetString("query", "general", "generator")
	if err != nil {
		return nil, errors.Wrap(err, "version: could not get json[query][general][generator]")
	}
	splitVersion := strings.Fields(rawSemanticVersion)
	if len(splitVersion) != 2 {
		return nil, errors.Wrapf(err, "version: invalid semantic version %v", rawSemanticVersion)
	}
	version, err := semver.NewVersion(splitVersion[1])
	if err != nil {
		return nil, errors.Wrap(err, "version: could not parse query")
	}

	return version, nil
}

// Namespaces returns a map[string]string
// containing the canonical names for a page's namespaces
// as keys and the localized names as values.
// This means that, for fr.wikipedia.org,
// Namespaces["Talk"] will return "Discussion"
// as the localized term for the Talk namespace.
func (w *Client) Namespaces() (map[string]string, error) {
	response, err := w.Get(map[string]string{
		"action": "query",
		"meta":   "siteinfo",
		"siprop": "namespaces",
	})
	if err != nil {
		return nil, errors.Wrap(err, "namespaces: could not send GET request")
	}

	namespaceArray, err := response.GetObject("query", "namespaces")
	if err != nil {
		return nil, errors.Wrap(err, "namespaces: could not get json[query][namespaces]")
	}

	allNamespaces := make(map[string]string)
	for _, nsValue := range namespaceArray.Map() {
		namespace, err := nsValue.Object()
		if err != nil {
			return nil, errors.Wrap(err, "namespaces: could not convert")
		}

		// each namepsace would look like this: (note: formatversion=2)
		// (from fr.wikipedia.org)
		// "1": {
		//        "id": 1,
		//        "case": "first-letter",
		//        "name": "Discussion",
		//        "subpages": true,
		//        "canonical": "Talk",
		//        "content": false,
		//        "nonincludable": false
		//     },

		localized, err := namespace.GetString("name")
		if err != nil {
			return nil, errors.Wrap(err, "namespaces: could not get json[name]")
		}
		if localized == "" {
			continue // Main namespace
		}
		canonical, err := namespace.GetString("canonical")
		if err != nil {
			return nil, errors.Wrap(err, "namespaces: could not get json[canonical]")
		}
		allNamespaces[canonical] = localized
	}

	return allNamespaces, nil
}

// LocalizedNamespace returns the localized version of some canonical namespace
// and an error
func (w *Client) LocalizedNamespace(canonical string) (string, error) {
	namespaces, err := w.Namespaces()
	if err != nil {
		return "", errors.Wrap(err, "localizedNamespace: could not get namespaces map")
	}

	localized, ok := namespaces[canonical]
	if !ok {
		return "", errors.Wrapf(err, "localizedNamespace: %v not a canonical namespace", canonical)
	}
	return localized, nil
}
