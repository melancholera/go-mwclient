# go-mwclient

go-mwclient is a [Go](https://golang.org) package for interacting with
the MediaWiki JSON API.

go-mwclient aims to be a thin wrapper around the MediaWiki API that
takes care of the most tedious parts of interacting with the API (such
as authentication and query continuation), but it does not aim to
abstract away all the functionality of the API.

The canonical import path for this package is

    import gitlab.com/melancholera/go-mwclient // imports "mwclient"

Documentation:

- GoDoc: <http://godoc.org/gitlab.com/melancholera/go-mwclient>
- MediaWiki API docs: <https://www.mediawiki.org/wiki/API:Main_page>

As of v1.0.0, go-mwclient uses [semantic versioning](http://semver.org/).
The `master` branch contains the most recent v1.x.x release.

## Installation

    go get -u gitlab.com/melancholera/go-mwclient

## Example

    package main

    import (
        "fmt"

        "gitlab.com/melancholera/go-mwclient"
    )

    func main() {
        // Initialize a *Client with New(), specifying the wiki's API URL
        // and your HTTP User-Agent. Try to use a meaningful User-Agent.
        w, err := mwclient.New("https://en.wikipedia.org/w/api.php", "myWikibot")
        if err != nil {
            panic(err)
        }

        // Log in.
        err = w.Login("USERNAME", "PASSWORD")
        if err != nil {
            panic(err)
        }

        // Specify parameters to send.
        parameters := map[string]string{
            "action":   "query",
            "list":     "recentchanges",
            "rclimit":  "2",
            "rctype":   "edit",
            "continue": "",
        }

        // Make the request.
        resp, err := w.Get(parameters)
        if err != nil {
            panic(err)
        }

        // Print the *jason.Object
        fmt.Println(resp)
    }

## Dependencies

Other than the standard library, go-mwclient depends on the following
third party, open source packages:

- <https://github.com/Masterminds/semver/v3> (MIT)
- <https://github.com/antonholmquist/jason> (MIT)
- <https://github.com/mrjones/oauth> (MIT)
- <https://github.com/pkg/errors> (BSD 2-Clause)
- <https://github.com/stretchr/testify> (MIT)

## Copyright

More information in [LICENSE.md](LICENSE.md) and [NOTICES.md](NOTICES.md).

MIT License

Copyright (c) 2019 Vibrio Melancholia and contributors

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

### Up to commit a3c2125a58513016d41ede1a0978f7a16f2612ca

To the extent possible under law, the author(s) have dedicated all
copyright and related and neighboring rights to this software to the
public domain worldwide. This software is distributed without any
warranty.

You should have received a copy of the CC0 Public
Domain Dedication along with this software. If not, see
<http://creativecommons.org/publicdomain/zero/1.0/>.

### params package

The `params` package is based on the `net/url` package from the Go
standard library, which is released under a BSD-style license. See
params/LICENSE.

Contributions to the `params` package as part of this project are
released to the public domain via CC0, as noted above and specified in
the COPYING file.

### `files.go` from github.com/sadbox/mediawiki

`files.go` contains code that has been taken from or at least inspired from
[github.com/sadbox/mediawiki](https://github.com/sadbox/mediawiki),
which is licensed under the MIT license.

### Licenses for other software used

This project would not have been possible if it weren't for the dependencies it uses.
Copyright information can be viewed in [NOTICES.md](NOTICES.md).
