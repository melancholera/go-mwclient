Contributions to go-mwclient can be submitted
through [merge requests on GitLab.](https://gitlab.com/melancholera/go-mwclient/merge_requests)

By submitting a merge request for this project,
you agree to license your contribution [under the MIT license](LICENSE.md) to this project

Before submitting a pull request, please check that the code works and that all
tests pass. If necessary, update existing tests.
