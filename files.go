package mwclient

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"net/http/httputil"
	"os"
	"strconv"
	"time"

	"github.com/antonholmquist/jason"
	"github.com/pkg/errors"
	"gitlab.com/melancholera/go-mwclient/params"
)

// Download contains code from github.com/sadbox/mediawiki.
// Licensed under the MIT license by James McGuire and Michael McConville.
// More information in LICENSE.md.

// Download creates a copy of a file to a ReadCloser and an applicable error.
// Note that filename should be, for instance, "Test.jpg" and NOT "File:Test.jpg"
// or "Fichier:Test.jpg". The namespace should be ommitted.
// The File namespace will be added automatically
// and normalization would convert page titles to their canonical form,
// that is, changing the namespace to their localized form.
func (w *Client) Download(filename string) (io.ReadCloser, error) {
	// handle empty string
	if filename == "" {
		return nil, errors.New("download: filename is empty")
	}

	// first get the appropriate page
	resp, err := w.Get(map[string]string{
		"action": "query",
		"format": "json",
		"prop":   "imageinfo",
		"titles": fmt.Sprintf("File:%v", filename),
		"iiprop": "url",
	})
	if err != nil {
		return nil, errors.Wrap(err, "download: could not send GET request")
	}

	queryList, err := resp.GetObjectArray("query", "pages")
	if err != nil {
		return nil, errors.Wrap(err, "download: could not get json[query][pages]")
	}

	if len(queryList) != 1 { // there should be only 1 query
		return nil, fmt.Errorf("download: query of length %v; there may be a `|` in the filename", len(queryList))
	}
	resp = queryList[0]

	// from here there should be an resp[imageinfo].
	imageInfo, err := resp.GetObjectArray("imageinfo")
	if err != nil || len(imageInfo) < 1 {
		// try getting an invalid reason
		if reason, errReason := resp.GetString("invalidreason"); errReason == nil {
			err = errors.Wrapf(err, "could not get image: %v", reason)
		}
		return nil, errors.Wrapf(err, "download: image %v not found", filename)
	}
	resp = imageInfo[0]

	// check url
	url, err := resp.GetString("url")
	if err != nil {
		return nil, errors.Wrap(err, "download: could not get json[url]")
	}

	// perform a Get request
	request, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, errors.Wrapf(err, "download: could not get %v", url)
	}
	request.Header.Set("User-Agent", w.UserAgent)

	downloadResp, err := w.httpc.Do(request)
	if err != nil {
		return nil, errors.Wrapf(err, "download: could not download %v", url)
	}
	return downloadResp.Body, nil
}

// DownloadToFile downloads File:remote to a file named local.
// Note that remote should be, for instance, "Test.jpg" and NOT "File:Test.jpg"
// or "Fichier:Test.jpg". The namespace should be ommitted.
// Returns an error if it could not download said file,
// if local is blank or invalid,
// or if local exists in the current folder, unless if overwrite is set to True.
// Local file will be overwritten if overwrite is set to True.
func (w *Client) DownloadToFile(remote, local string, overwrite bool) error {
	// check if file exists
	exists := func(local string) bool {
		if _, err := os.Stat(local); err != nil {
			if os.IsNotExist(err) {
				return false
			}
		}
		return true
	}
	if exists(local) && !overwrite {
		return fmt.Errorf("downloadToFile: file %v exists and overwrite is false", local)
	}

	gotFile, err := w.Download(remote)
	if err != nil {
		return errors.Wrapf(err, "downloadToFile: could not get %v", remote)
	}

	// now open
	outFile, err := os.Create(local)
	if err != nil {
		return errors.Wrapf(err, "downloadToFile: could not create %v", local)
	}
	_, err = io.Copy(outFile, gotFile)
	if err != nil {
		return errors.Wrapf(err, "downloadToFile: could not save to %v", local)
	}

	if outFile.Close() != nil {
		return errors.Wrapf(err, "could not close %v", local)
	}

	return nil
}

// Upload performs a simple upload of a file to some filename
// and returns the response.
// Note that this will not provide page contents and edit summary,
// among other parameters that should be set manually in parameters.
// The p (params.Values) argument should contain parameters from:
//	https://www.mediawiki.org/wiki/API:Edit#Parameters.
// p example:
//	params.Values{
//		"filename":       "Some file.jpg",       // "File:" will be added anyway; will override filename
//		"comment":        "Uploaded osomething", // will override default upload comment
//		"text":           "Look at this file I uploaded!",
//		"ignorewarnings": "",  // ignorewarnings specified means true even if blank
//	}
// If p is nil then it is as if no optional parameters were passed.
// If p["filename"] exists then it overrides filename.
// Upload will set the 'action' and 'token' parameters automatically, but if the
// token field in p is non-empty, Upload will not override it.
// Setting p["url"], p["chunk"], p["filekey"], or p["sessionkey"] may yield undefined behavior.
// The default comment is "Uploaded via w.UserAgent".
func (w *Client) Upload(file io.Reader, filename string, p params.Values) (*jason.Object, error) {
	// check if filename is nada
	if filename == "" {
		return nil, errors.New("upload: filename is empty")
	}

	// callf is for maxlag.
	callf := func() (io.ReadCloser, error) {
		// get a token first
		if p == nil {
			p = make(params.Values)
		}
		var err error
		if p["token"] == "" {
			p["token"], err = w.PostToken(CSRFToken)
			if err != nil {
				err = errors.Wrap(err, "upload: could not get CSRF token")
				return nil, err
			}
		}
		if _, set := p["filename"]; !set {
			p.Set("filename", filename)
		}
		if _, set := p["comment"]; !set {
			p.Set("comment", fmt.Sprintf("Uploaded via %v", w.UserAgent))
		}

		p.Set("action", "upload")
		p.Set("format", "json")
		p.Set("formatversion", "2")

		// write to a multipart "file"
		buffer := &bytes.Buffer{}
		writer := multipart.NewWriter(buffer)
		for key, val := range p {
			err = writer.WriteField(key, val)
			if err != nil {
				err = errors.Wrapf(err, "upload: could not write %v:%v into multipart.Writer", key, val)
				return nil, err
			}
		}

		// now for the file
		part, err := writer.CreateFormFile("file", filename)
		_, err = io.Copy(part, file)
		if err != nil {
			err = errors.Wrapf(err, "upload: could not copy file to multipart section")
			return nil, err
		}
		err = writer.Close()
		if err != nil {
			err = errors.Wrap(err, "writer: could not close file")
			return nil, err
		}

		// POST this bread.
		req, err := http.NewRequest("POST", w.apiURL.String(), buffer)
		if err != nil {
			err = errors.Wrap(err, "writer: could not send POST request")
			return nil, err
		}

		// Set headers
		req.Header.Set("User-Agent", w.UserAgent)
		req.Header.Set("Content-Type", writer.FormDataContentType())
		if w.debug != nil {
			reqdump, err := httputil.DumpRequestOut(req, true)
			if err != nil {
				fmt.Fprintf(w.debug, "Err dumping request: %v\n", err)
			} else {
				w.debug.Write(reqdump)
			}
		}

		// Make the request
		resp, err := w.httpc.Do(req)
		if err != nil {
			err = errors.Wrap(err, "writer: error occured during HTTP request")
			return nil, err
		}

		if w.debug != nil {
			respdump, err := httputil.DumpResponse(resp, true)
			if err != nil {
				fmt.Fprintf(w.debug, "Err dumping response: %v\n", err)
			} else {
				w.debug.Write(respdump)
			}
		}

		// Handle maxlag
		if resp.Header.Get("X-Database-Lag") != "" {
			defer resp.Body.Close()
			retryAfter, err := strconv.Atoi(resp.Header.Get("Retry-After"))
			if err != nil {
				return nil, err
			}

			body, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				return nil, err
			}

			return nil, maxLagError{
				string(body),
				retryAfter,
			}
		}

		return resp.Body, nil
	}
	var reqResp io.ReadCloser
	var err error

	if w.Maxlag.On {
		for tries := 0; tries < w.Maxlag.Retries; tries++ {
			reqResp, err = callf()

			// Logic for handling maxlag errors. If err is nil or a different error,
			// they are passed through in the else.
			if lagerr, ok := err.(maxLagError); ok {
				// If there are no tries left, don't wait needlessly.
				if tries < w.Maxlag.Retries-1 {
					w.Maxlag.sleep(time.Duration(lagerr.Wait) * time.Second)
				}
				continue
			} else {
				break
			}
		}
		reqResp, err = nil, ErrAPIBusy
	} else {
		// If maxlag is not enabled, just do the request regularly.
		reqResp, err = callf()
	}

	// adapted from callJSON
	if err != nil {
		err = errors.Wrap(err, "upload: uploading failed")
	}
	if reqResp != nil {
		defer reqResp.Close()
	}

	js, err := jason.NewObjectFromReader(reqResp)
	if err != nil {
		return nil, errors.Wrap(err, "upload: could not read JSON object")
	}

	return js, extractAPIErrors(js)
}
