module gitlab.com/melancholera/go-mwclient

go 1.13

require (
	github.com/Masterminds/semver/v3 v3.0.3
	github.com/antonholmquist/jason v1.0.0
	github.com/mrjones/oauth v0.0.0-20190623134757-126b35219450
	github.com/pkg/errors v0.8.1
	github.com/stretchr/testify v1.4.0
)
