package mwclient

import (
	"testing"

	"github.com/Masterminds/semver/v3"
	"github.com/stretchr/testify/assert"
)

func TestClient_Version(t *testing.T) {
	assert := assert.New(t)
	// how do you test these?
	// Well... As of 20191228T024840Z, Wikipedia's at version MediaWiki 1.35.0-wmf.11.
	// Doubt that they would revert to an older version.
	// But just in case compare it to version 1.10
	const APIUrl = "https://en.wikipedia.org/w/api.php"
	ToCompare := semver.MustParse("1.10")
	client, err := New(APIUrl, DefaultUserAgent)
	assert.NoError(err, "Version: no error for New")

	version, err := client.Version()
	assert.NoError(err, "Version: no error for Version")
	assert.GreaterOrEqualf(version.Compare(ToCompare), 0, "Version: %v must be at least 1.10", version)
}

func TestClient_Namespaces(t *testing.T) {
	assert := assert.New(t)
	// Let's use the French Wikipedia for this example
	const APIUrl = "https://fr.wikipedia.org/w/api.php"
	client, err := New(APIUrl, DefaultUserAgent)
	assert.NoError(err, "Namespaces: no error for New")

	namespaces, err := client.Namespaces()
	assert.NoError(err, "Namespaces: no error for Namespaces")
	assert.Contains(namespaces, "File", "Namespaces: should contain File")
	assert.Equal("Fichier", namespaces["File"], "Namespaces: File -> Fichier")
}
